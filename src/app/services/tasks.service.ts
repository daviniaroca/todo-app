import { Injectable, OnInit } from '@angular/core';
import { Task, TaskClass } from '../interfaces/task';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TasksService implements OnInit {
  private rows$ = new Subject<any[]>();
  private url = "http://localhost:3000/tasks"

  constructor(private httpClient: HttpClient) {

  }
  ngOnInit(): void {

  }

  getTaskList(): Observable<any> {
    this.httpClient.get(this.url).subscribe((rows: Task[]) => {
      this.rows$.next(rows);
    })
       return this.rows$.asObservable();

  }

  deleteRow(row: TaskClass) {
    this.httpClient
    .delete(`${this.url}/${row.id}`)
    .subscribe((rows: Task[]) => {
      this.rows$.next(rows);
    });
  }

  updateRow(row: TaskClass) {
    this.httpClient
      .put(`${this.url}/${row.id}`, {task: row})
      .subscribe((rows: Task[]) => {
        this.rows$.next(rows);
      });
  }

  addTask(task: TaskClass) {
    this.httpClient.
    post(this.url, {task: task})
    .subscribe((rows: Task[]) => {
      this.rows$.next(rows);
    });
  }

}
