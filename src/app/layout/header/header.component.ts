import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'todo-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'My TODO list';
  constructor() {

  }

  ngOnInit(): void {
  }

}
