import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { Task } from 'src/app/interfaces/task';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'todo-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  public rowData$: Observable<any> = null;
  lineChartData: ChartDataSets[] = [
    { data: [], label: "Number of Events", fill: false }
  ];
  lineChartLabels: Label[]=  [];
  lineChartOptions = {
    responsive: true,
  };
  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
  constructor(private taskService: TasksService) {

}

  ngOnInit(): void {
    this.taskService.getTaskList().subscribe(rows => {
      this.rowData$ = rows;
      const doneSortedTasks: Task[] = rows.filter(r => r.done === true).sort((a: Task, b: Task) => {
        // sort by most recently done
        return new Date(b.dateDone).getTime() - new Date(a.dateDone).getTime()
      });
      const chars = doneSortedTasks.map(task=> task.desc.length);
      this.lineChartData = [
        { data: chars, label: 'Chars by task' },
      ];
      const label : Label[]= doneSortedTasks.map(task=> (new Date(task.dateDone)).toLocaleDateString('en-US'));
      this.lineChartLabels= label;
    });
  }

}
