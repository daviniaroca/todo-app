import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Task, TaskClass } from 'src/app/interfaces/task';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'todo-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  public desc : string = '';
  public taskCount: number;
  constructor(private taskService: TasksService) { }

  ngOnInit(): void {
  }

  saveTask(){
    const task = new TaskClass({desc: this.desc, done: false, id: 0, dateDone: null})
    this.taskService.addTask(task);
    this.desc = '';
  }
}
