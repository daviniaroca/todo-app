import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoListComponent } from './todo-list/todo-list.component';

import { RouterModule } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TasksService } from '../services/tasks.service';
import { ChartsModule } from 'ng2-charts';
import { ChartComponent } from './chart/chart.component';


@NgModule({
  declarations: [HeaderComponent, TodoFormComponent, TodoListComponent, ChartComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    FormsModule,
    ChartsModule
  ],
  providers: [ TasksService ],
  exports: [HeaderComponent, TodoFormComponent, TodoListComponent, ChartComponent]
})
export class LayoutModule { }
