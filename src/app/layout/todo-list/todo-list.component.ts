import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { TasksService } from 'src/app/services/tasks.service';
import { Observable } from 'rxjs';
import { Task, TaskClass } from 'src/app/interfaces/task';

@Component({
  selector: 'todo-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})


export class TodoListComponent implements OnInit {
  @Input()
  filterTask: boolean;

  @ViewChild('agGrid') agGrid: AgGridAngular;
  public columnDefs = [
    { field: 'desc', checkboxSelection: true }
  ];
  public defaultColDef: any;
  public rowData$: Observable<any> = null;

  constructor(private taskService: TasksService) { }

  ngOnInit(): void {
    this.defaultColDef = {
      flex: 1,
      minWidth: 110,
      editable: !this.filterTask,
      resizable: true,
    };
    this.taskService.getTaskList().subscribe(rows => {
      const auxRows = rows.filter(r => r.done === this.filterTask);
      console.log(this.filterTask, 'un list');
      if (this.filterTask === true) {
        this.rowData$ = auxRows.sort((a: Task, b: Task) => {
          // sort by most recently done
          return new Date(b.dateDone).getTime() - new Date(a.dateDone).getTime()
        });
      } else {
        this.rowData$ = auxRows;
      }


    });

  }

  cellEditingStopped(event: any) {
    const editedTask : Task = event.data;
    const updatedTask = new TaskClass(editedTask);
    this.taskService.updateRow(updatedTask);
  }

  public moveToDoneList() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    let selectedData: Task = selectedNodes.map(node => node.data)[0];
    const updatedTask = new TaskClass(selectedData);
    updatedTask.dateDone = new Date();
    updatedTask.done = true;
    this.taskService.updateRow(updatedTask);
  }

  public deleteFromList() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData: Task = selectedNodes.map(node => node.data)[0];
    const taskToDelete = new TaskClass(selectedData);
    this.taskService.deleteRow(taskToDelete);
  }

}
