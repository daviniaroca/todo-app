export interface Task {
  desc: string;
  done: boolean;
  id: number;
  dateDone: Date | undefined;
}

export class TaskClass implements Task {
  desc: string;
  done: boolean;
  id: number;
  dateDone: Date;
  constructor(task: Task){
    this.desc = task.desc;
    this.done = task.done;
    this.id = task.id;
    this.dateDone = new Date(task.dateDone);
  }
}

