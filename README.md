# TodoApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## For the ExpressServer

Run `cd backend` and then run `npm run start`.

## FeathersJS

I did not have enough time to dig into FeatherJS, so I just installed the ExpressJS package that uses Feathers but used the regular Express syntax for the routes. I mocked the DataBase as a static variable `rows` in the `database.js` file as a starter point.
