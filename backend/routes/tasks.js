var express = require('express');
var data = require('../mockApi/database');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(data);
});

router.post('/', function(req, res, next) {
  const task = req.body.task;
  const newTask = {
    desc: task.desc,
    done : false,
    dateDone: undefined,
    id: data[0].id +1
  };
  data.unshift({...newTask});
  res.send(data);
});

// update row
router.put('/:id', function(req, res, next) {
  const task = req.body.task;
  data.filter(row =>
    row.id === Number(req.params.id)
  ).map(modRow => {
    modRow.desc = task.desc;
    modRow.done = task.done;
    modRow.id = task.id;
    modRow.dateDone = task.dateDone});
  res.send(data);
});

router.delete('/:id', function(req, res, next) {
  data = data.filter(row =>
    row.id !== Number(req.params.id)
  );
  res.send(data);
});

module.exports = router;
